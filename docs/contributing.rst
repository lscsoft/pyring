Contributing
--------------

If you have a request for an additional feature, spot any mistake or find any problem with using the code, please open an issue. If you are part of the LVK collaboration, this can be done through the `issue tracker <https://git.ligo.org/lscsoft/pyring/-/issues>`_, otherwise, please send an email to our `Service Desk <https://git.ligo.org/lscsoft/pyring/-/issues/service_desk>`_ address (`contact+lscsoft-pyring-10295-issue-@support.ligo.org`) to open an issue.

To develop the code, we follow a standard {create a branch-apply your edits-submit a merge request} workflow.
If you wish to contribute directly to its development, please follow the steps described below:

1. Make sure to start from the latest version of the branch you are looking at, so please pull before planning any changes to the code;
2. Create your own branch (if you have permissions below developer, please fork the code and then keep following these instructions), where you can apply all your improvements and modifications to the code;
3. Please check thoroughly your changes. Specifically check that, after applying them, you are still able to install and run the code without occurring in any errors. Test it by running a couple of quick examples (e.g. config_gw150914_local_data.ini, config_damped_sinusoids_inj_gaussian_noise.ini).
4. Create a merge request to master and assign it to either Gregorio (`gregorio.carullo@ligo.org`) or Danny (`danny.laghi@ligo.org`);

Someone will have a look at your edits and eventually merge them.
Thanks for taking time to contribute to the code, your help is greatly appreciated!

For additional questions, feedback and suggestions feel free to reach by email to `gregorio.carullo@ligo.org` or, if you are part of the LVK collaboration, directly through `chat.ligo.org` on the `pyRing` channel. 