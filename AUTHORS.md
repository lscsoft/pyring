Authors and contributors
------------------------

Primary authors: Gregorio Carullo, Walter Del Pozzo, John Veitch

Contributors (chronological order):

Danny Laghi,
Max Isi,
Duncan Macleod,
Vasco Gennari,
Will Farr,
Giada Caneva,
Simon Maenaut.
