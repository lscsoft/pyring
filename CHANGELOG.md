List of changes since first released version(v2.0.0):

* v2.0.1: new time-axis, cleanup of data reading/generation.
* v2.1.0: add TEOB-PM and massively parallel CPNest.
* v2.2.1: add quadratic modes and tails.
* v2.3.0: add raynest integration, improve packaging and documentation.
* v2.4.0: add TEOBPM injections, fix requirements, improve plots and functions cleanup.
* v2.5.0: remove superseded matplotlib version requirement
* v2.6.0: MMRDNP tref fix, TEOB cleanup, sphinx and pipeline updates, imr samples options addition, plots improvements
