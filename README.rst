|pypi| |conda| |version| |platforms| |pipeline status| |license| |DOI| 

======
pyRing
======

Time-domain Bayesian inference software package targeting the analysis of ringdown signals.

For installation instructions, documentation, tutorials and more, visit `the documentation <https://lscsoft.docs.ligo.org/pyring/>`__.

Support and contributions can be submitted via:

- Email to the support desk: contact+lscsoft-pyring-10295-issue-@support.ligo.org
- `Issue tracker <https://git.ligo.org/lscsoft/pyring/-/issues>`__ for members of the LVK collaboration.
- The  `pyRing chat <https://chat.ligo.org/ligo/channels/pyRing>`__ for members of the LVK collaboration.

.. |pypi| image:: https://badge.fury.io/py/pyRingGW.svg
   :target: https://pypi.org/project/pyRingGW
.. |conda| image:: https://anaconda.org/conda-forge/pyringgw/badges/version.svg
   :target: https://anaconda.org/conda-forge/pyringgw
.. |platforms| image:: https://anaconda.org/conda-forge/pyringgw/badges/platforms.svg
   :target: https://github.com/conda-forge/pyringgw-feedstock
.. |pipeline status| image:: https://git.ligo.org/lscsoft/pyRing/badges/master/pipeline.svg
   :target: https://git.ligo.org/lscsoft/pyRing/commits/master
.. |version| image:: https://img.shields.io/pypi/pyversions/pyRingGW.svg
   :target: https://pypi.org/project/pyRingGW/
.. |license| image:: https://img.shields.io/badge/License-MIT-red.svg
   :target: https://opensource.org/licenses/MIT
.. |DOI| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.8165508.svg
   :target: https://doi.org/10.5281/zenodo.8165508
